package com.orangemarshall.olympics.pickableitems;

import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.inventory.ItemStack;

/*
    Floating item (armorstand with item as helmet).
 */

public class FloatingItem{

    protected ArmorStand armorStand;

    public FloatingItem(Location location, ItemStack itemStack){
        armorStand = location.getWorld().spawn(location, ArmorStand.class);
        armorStand.setVisible(false);
        armorStand.setGravity(false);
        armorStand.setSmall(true);
        armorStand.setHelmet(itemStack);
    }

    public void setSmall(boolean small){
        armorStand.setSmall(small);
    }

    public void setName(String name){
        if(name == null || name.isEmpty()){
            hideName();
        }else{
            showName(name);
        }
    }

    public ArmorStand getBukkitEntity(){
        return armorStand;
    }

    private void hideName(){
        armorStand.setCustomNameVisible(false);
    }

    private void showName(String name){
        armorStand.setCustomNameVisible(true);
        armorStand.setCustomName(name);
    }

    public void remove(){
        armorStand.remove();
    }

}
