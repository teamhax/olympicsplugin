package com.orangemarshall.olympics.pickableitems;

import com.orangemarshall.olympics.Main;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/*
    Listens for items to be picked up.
 */

public class PickableItemListener implements Listener{

    private static final List<PickableItem> ITEMS = new ArrayList<>();

    //TODO this sucks
    public PickableItemListener(){
        Main.instance().register(this);
    }

    public static void registerItem(PickableItem item){
        ITEMS.add(item);
    }

    public static void unregisterItem(PickableItem item){
        ITEMS.remove(item);
    }

    public static void unregisterAll(){
        List<PickableItem> items = new ArrayList<>(ITEMS);
        items.forEach(item -> {
            unregisterItem(item);
            item.remove();
        });
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event){
        List<PickableItem> items = ITEMS.stream()
                .filter(new PickableHitboxIntersectionFilter(event.getPlayer())).collect(Collectors.toList());
        items.forEach(item -> item.onPickup(event.getPlayer()));
    }


    class PickableHitboxIntersectionFilter implements Predicate<PickableItem>{

        private Entity entity;

        public PickableHitboxIntersectionFilter(Entity entity){
            this.entity = entity;
        }

        public PickableHitboxIntersectionFilter(PickableItem item){
            this(item.getBukkitEntity());
        }

        @Override
        public boolean test(PickableItem testEntity){
            return ((CraftEntity)entity).getHandle().getBoundingBox().b(((CraftEntity)testEntity.getBukkitEntity()).getHandle().getBoundingBox());
        }

    }

}
