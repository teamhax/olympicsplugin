package com.orangemarshall.olympics.pickableitems;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/*
    Floating items that can be picked up by players.
 */

public class PickableItem extends FloatingItem{

    private static final PickableItemListener LISTENER = new PickableItemListener();

    public PickableItem(Location location, ItemStack itemStack){
        super(location, itemStack);
        LISTENER.registerItem(this);
    }

    @Override
    public void remove(){
        super.remove();
        LISTENER.unregisterItem(this);
    }

    public void onPickup(Player player){
        remove();
    }

}

