package com.orangemarshall.olympics;

import com.orangemarshall.olympics.disciplines.CommandExecutor;
import com.orangemarshall.olympics.disciplines.IDiscipline;
import com.orangemarshall.olympics.disciplines.archery.ArcheryDiscipline;
import com.orangemarshall.olympics.disciplines.sprinting.SprintingDiscipline;
import com.orangemarshall.olympics.disciplines.swimming.SwimmingDiscipline;
import com.orangemarshall.olympics.listeners.*;
import com.orangemarshall.olympics.utils.FireworkSpawner;
import com.orangemarshall.olympics.utils.GameNotifier;
import com.orangemarshall.olympics.utils.GlobalPlayerScore;
import com.orangemarshall.olympics.utils.ScoreboardRunnable;
import org.bukkit.*;
import org.bukkit.entity.ArmorStand;
import org.bukkit.scoreboard.Scoreboard;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.stream.Collectors;

//TODO First level up sound doesn't play?
//TODO Fireworks on end?
//TODO only startCountdown on command?

//TODO message on join explaining game
//TODO info message on discipline start

//TODO integrate cooldown on disciplines

//TODO adjust cooldown for disciplines

public class Main extends BasicPlugin{

    private boolean hasStarted;
    private Manager manager;
    private IDiscipline currentDiscipline;
    private Queue<IDiscipline> disciplineQueue;
    private World world;

    private static Main instance;

    public Main(){
        instance = this;
        disciplineQueue = new LinkedList<>();
        manager = new Manager();
    }

    public String getGameName(){
        return "MC-lympics";
    }

    @Override
    public void onEnable(){
        world = Bukkit.getWorld("world");
        getCommand("startolympics").setExecutor(new CommandExecutor());
    }

    public boolean startOlympics(){
        if(hasStarted){
            return false;
        }else{
            hasStarted = true;
            cleanUp();
            loadConfig();
            loadListeners();
            loadDisciplines();
            startGame();
            return true;
        }
    }

    public void nextDiscipline(){
        if(currentDiscipline != null){  // End previous discipline
            manager.endDiscipline();
            currentDiscipline.end();
        }
        if(!disciplineQueue.isEmpty()){ // Start next discipline
            currentDiscipline = disciplineQueue.poll();
            currentDiscipline.startCountdown();
            manager.startDiscipline();
        }else{                          // Game has ended
            currentDiscipline = null;
            endGame();
        }
    }

    private void startGame(){
        Bukkit.broadcastMessage(ChatColor.YELLOW.toString() + ChatColor.BOLD + "The Olmypics have started!");
        if(!disciplineQueue.isEmpty()){
            manager.startDiscipline();
            currentDiscipline = disciplineQueue.poll();
            currentDiscipline.startCountdown();
        }
    }

    private void endGame(){
        sendFinishScoreboard(); //TODO just a placeholder
        sendFinishMessage();
    }

    public void addToDisciplineQueue(IDiscipline discipline){
        disciplineQueue.add(discipline);
    }

    private void loadDisciplines(){
        //disciplineQueue.add(new InitialCountdown(this, "Sprinting", 10));   // Add locations for finish line
        disciplineQueue.add(new SprintingDiscipline(this, "disciplines.sprinting.spawn", "disciplines.sprinting.boostpowerups",
                "disciplines.sprinting.leappowerups", "disciplines.sprinting.speedpowerups",
                "disciplines.sprinting.finishline_location1", "disciplines.sprinting.finishline_location2"));
        //disciplineQueue.add(new InitialCountdown(this, "Archery", 10));
        disciplineQueue.add(new ArcheryDiscipline(this, "disciplines.archery.base_location"));
        //disciplineQueue.add(new InitialCountdown(this, "Swimming", 10));
        disciplineQueue.add(new SwimmingDiscipline(this, "disciplines.swimming.boundingbox_location_low", "disciplines.swimming.boundingbox_location_high"));
        //TODO More to add
    }

    private void cleanUp(){
        world.getEntitiesByClass(ArmorStand.class).forEach(ArmorStand::remove);
        world.setDifficulty(Difficulty.EASY);
    }

    private void loadConfig(){
        getConfig().options().copyDefaults(true);
        saveDefaultConfig();
    }

    public static Main instance(){
        return instance;
    }

    public IDiscipline getCurrentDiscipline(){
        return currentDiscipline;
    }

    private void sendFinishScoreboard(){
        Bukkit.getOnlinePlayers().forEach(player -> {
            int score = GlobalPlayerScore.getPlayerScore(player).getScore();
            Scoreboard scoreboard = new ScoreboardRunnable.ScoreboardBuilder(ChatColor.YELLOW + ChatColor.BOLD.toString() + "MC-lympics")
                    .setLine(14, ChatColor.WHITE.toString() + "Name: ")
                    .setLine(13, ChatColor.AQUA + player.getName())
                    .setLine(11, ChatColor.WHITE.toString() + "Info: ")
                    .setLine(10, ChatColor.RED + "Finished!")
                    .setLine(7, ChatColor.GREEN.toString() + ChatColor.BOLD + "You finished")
                    .setLine(6, ChatColor.GREEN.toString() + ChatColor.BOLD + "the Olympics" + (score == 0 ? "!" : ""))
                    .setLine(5, score == 0 ? "" : ChatColor.GREEN.toString() + ChatColor.BOLD + "with " + score + " stars!")
                    .setLine(4, ChatColor.GREEN.toString() + ChatColor.BOLD + "Congrats!")
                    .setLine(1, ChatColor.YELLOW + "www.foundation.tf")
                    .build();

            player.setScoreboard(scoreboard);
        });
    }

    private void sendFinishMessage(){
        List<GlobalPlayerScore> players = GlobalPlayerScore.getAll().stream()
                .map(Map.Entry::getValue)
                .filter(score -> score.getScore() != 0)
                .sorted((score1, score2) -> Integer.compare(score2.getScore(), score1.getScore()))
                .limit(3)
                .collect(Collectors.toList());

        String[] strings = {
                "               " + ChatColor.YELLOW.toString() + ChatColor.BOLD + "1st place" + ChatColor.GRAY + " - " + ChatColor.YELLOW + "%name%" + ChatColor.GRAY + " - " + ChatColor.BOLD + "%stars% Stars",
                "               " + ChatColor.GOLD.toString() + ChatColor.BOLD + "2nd place" + ChatColor.GRAY + " - " + ChatColor.GOLD + "%name%" + ChatColor.GRAY + " - " + ChatColor.BOLD + "%stars% Stars",
                "               " + ChatColor.RED.toString() + ChatColor.BOLD + "3rd place" + ChatColor.GRAY + " - " + ChatColor.RED + "%name%" + ChatColor.GRAY + " - " + ChatColor.BOLD + "%stars% Stars"
        };

        GameNotifier notifier = GameNotifier.newNotifier(getName());

        if(players.size() == 0){
            notifier.addLine(ChatColor.RED.toString() + ChatColor.BOLD + "               No one has scored!");
        }else{
            for(int i = 0; i < 3 && i < players.size(); i++){
                GlobalPlayerScore score = players.get(i);
                if(i == 0){
                    FireworkSpawner.spawnRandomFireworks(score.getPlayer(), 20);
                }
                String message = strings[i].replace("%name%", score.getPlayer().getName()).replace("%stars%", score.getScore() + "");
                notifier.addLine(message);
            }
        }

        notifier.print();
    }

    private void loadListeners(){
        register(new DisableItemsListener());
        register(new DisableSuicideListener());
        register(new InvincibilityListener(world));
        register(new WorldProtectionListener());
        register(new ClearWeatherListener(world));
        register(new FriendlyWorldListener());
        register(new GamemodeListener(GameMode.ADVENTURE));
        register(new DisableOpenGuiListener());
        register(new FoodBarListener());
    }
}
