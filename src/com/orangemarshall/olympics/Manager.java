package com.orangemarshall.olympics;

import com.orangemarshall.olympics.utils.ScoreboardRunnable;
import com.orangemarshall.olympics.utils.TickRunnable;

public class Manager {

    private ScoreboardRunnable scoreboardRunnable;
    private TickRunnable  tickRunnable;

    public void startDiscipline(){
        tickRunnable = new TickRunnable();
        scoreboardRunnable = new ScoreboardRunnable(tickRunnable);
        tickRunnable.start();
        scoreboardRunnable.start();
    }

    public void endDiscipline(){
        scoreboardRunnable.cancel();
        tickRunnable.cancel();
        scoreboardRunnable = null;
        tickRunnable = null;
    }
}
