package com.orangemarshall.olympics.disciplines;

public interface ITickEvent{

    void onTick(int totalTicks);

}
