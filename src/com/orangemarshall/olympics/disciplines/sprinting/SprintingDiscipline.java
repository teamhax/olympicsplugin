package com.orangemarshall.olympics.disciplines.sprinting;

import com.orangemarshall.olympics.Main;
import com.orangemarshall.olympics.disciplines.Discipline;
import com.orangemarshall.olympics.disciplines.sprinting.powerups.BoostPowerup;
import com.orangemarshall.olympics.disciplines.sprinting.powerups.LeapPowerup;
import com.orangemarshall.olympics.disciplines.sprinting.powerups.SpeedPowerup;
import com.orangemarshall.olympics.listeners.MovePreventionListener;
import com.orangemarshall.olympics.listeners.PlayerEntersAreaListener;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class SprintingDiscipline extends Discipline{

    private HashMap<Player, Integer> placing = new HashMap<>();
    private int playersFinished = 0;

    public SprintingDiscipline(Main plugin, String... args){
        super(plugin, args);
    }

    @Override
    protected void preparePlayers(){
        super.preparePlayers();
        Location spawnLocation = toMiddle(getLocation(plugin.getConfig().getString(args[0])));
        spawnLocation.setDirection(new Vector(1, 0, 0));
        Bukkit.getOnlinePlayers().forEach(player -> {
            placing.put(player, -1);
            player.teleport(spawnLocation);
            spawnLocation.add(0, 0, -1);
        });
    }

    @Override
    protected void loadListeners(){
        List<String> boost = plugin.getConfig().getStringList(args[1]);
        List<String> leap = plugin.getConfig().getStringList(args[2]);
        List<String> speed = plugin.getConfig().getStringList(args[3]);

        boost.forEach(key -> new BoostPowerup(toMiddle(getLocation(key)), BoostPowerup.Direction.EAST));
        leap.forEach(key -> new LeapPowerup(toMiddle(getLocation(key))));
        speed.forEach(key -> new SpeedPowerup(toMiddle(getLocation(key))));

        String[] spl = plugin.getConfig().getString(args[4]).split(",");
        Location location1 = new Location(Bukkit.getWorld(spl[0]), Integer.parseInt(spl[1]), Integer.parseInt(spl[2]), Integer
                .parseInt(spl[3]));

        spl = plugin.getConfig().getString(args[5]).split(",");
        Location location2 = new Location(Bukkit.getWorld(spl[0]), Integer.parseInt(spl[1]), Integer.parseInt(spl[2]), Integer
                .parseInt(spl[3]));

        listeners.add(new PlayerEntersAreaListener(this::hasFinished, location1, location2));
        countdownListeners.add(new MovePreventionListener(true));
    }

    @Override
    public String getName(){
        return "Sprinting";
    }

    @Override
    public String getInfo(){
        return "   Try to be the first to reach the\n" +
               "  finish line! You will find powerups\n" +
               "        on the way to help you!";
    }

    @Override
    public void addGlobalScore(){
        //Take into account no one finishing (time ran out)

        int i = 3;

        List<Player> top3 = placing.entrySet()
                .stream()
                .filter(key -> key.getValue() > 0)
                .sorted()
                .limit(3)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        List<Player> noScore = placing.entrySet()
                .stream()
                .filter(key -> key.getValue() <= 0)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        for(Player player : top3){
            addGlobalScore(player, i--);
        }

        for(Player player : noScore){
            player.sendMessage(ChatColor.RED + "You didn't finish, so you will not be rewarded for this game!");
        }
    }

    public void hasFinished(Player player){
        placing.put(player, ++playersFinished);
        if(shallFinishGame()){
            plugin.nextDiscipline();
        }
    }

    private boolean shallFinishGame(){
        long finished = placing.values().stream().filter(value -> value.intValue() > 0).count();
        return finished >= 3 || finished == placing.values().size();    //Either all started have finished or 3 players finished
    }

    @Override
    public int getDuration(){
        return 20 * 240;
    }

    public Location getLocation(String key){
        String[] spl = key.split(",");
        return new Location(Bukkit.getWorld(spl[0]), Integer.parseInt(spl[1]), Integer.parseInt(spl[2]), Integer.parseInt(spl[3]));
    }

    public Location toMiddle(Location location){
        location.setX(location.getBlockX() + 0.5);
        location.setZ(location.getBlockZ() + 0.5);
        return location;
    }

}
