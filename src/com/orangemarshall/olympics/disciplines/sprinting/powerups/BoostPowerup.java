package com.orangemarshall.olympics.disciplines.sprinting.powerups;

import com.orangemarshall.olympics.pickableitems.PickableItem;
import com.orangemarshall.olympics.utils.Notifier;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;


public class BoostPowerup extends PickableItem{

    private float power = 2f;
    private Vector direction;

    public BoostPowerup(Location location, Direction direction){
        super(location, new ItemStack(Material.WOOL, 0, (short)0, (byte)12));
        this.setName(ChatColor.GREEN.toString() + ChatColor.BOLD + "BOOST");
        this.direction = direction.getVector();
    }

    @Override
    public void onPickup(Player player){
        super.onPickup(player);
        Notifier.newNotifier()
                .toPlayer(player)
                .withSound(Sound.ORB_PICKUP)
                .send(ChatColor.GREEN + "Picked up a " + ChatColor.GOLD.toString() + ChatColor.BOLD + "Boost Powerup" + ChatColor.GREEN + " !");
        player.setVelocity(direction.add(new Vector(0,1,0)).multiply(power));
    }

    public enum Direction{

        WEST(new Vector(-1, 0, 0)),
        EAST(new Vector(1, 0 ,0)),
        NORTH(new Vector(0, 0, -1)),
        SOUTH(new Vector(0, 0, 1));

        private Vector vector;

        Direction(Vector vector){
            this.vector = vector;
        }

        public Vector getVector(){
            return vector.clone();
        }
    }

}
