package com.orangemarshall.olympics.disciplines.sprinting.powerups;

import com.orangemarshall.olympics.pickableitems.PickableItem;
import com.orangemarshall.olympics.utils.Notifier;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


public class LeapPowerup extends PickableItem{

    private int duration = 20 * 10;

    public LeapPowerup(Location location){
        super(location, new ItemStack(Material.WOOL, 0, (short)0, (byte)8));
        this.setName(ChatColor.YELLOW.toString() + ChatColor.BOLD + "LEAP");
    }

    @Override
    public void onPickup(Player player){
        super.onPickup(player);
        Notifier.newNotifier()
                .toPlayer(player)
                .withSound(Sound.ORB_PICKUP)
                .send(ChatColor.GREEN + "Picked up a " + ChatColor.GOLD.toString() + ChatColor.BOLD + "Leap Powerup" + ChatColor.GREEN + " !");
        player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, duration, 2));
    }

}
