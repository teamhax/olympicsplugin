package com.orangemarshall.olympics.disciplines.archery;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.HashMap;
import java.util.Optional;

/*
    Stores scores for up to 5 shots on a target for each player.
    Provides bounds for target distance.
    Also provides the location of the target for each player.
 */

public class ArcheryScore{

    private static final HashMap<Player, ArcheryScore> players = new HashMap<>();

    public static final int MAX_SHOTS = 5;
    public static final int MIN_DISTANCE = 10;
    public static final int MAX_DISTANCE = 20;

    private Player player;
    private Integer[] scores;
    private Location location;

    public ArcheryScore(Player player, Location location){
        players.put(player, this);
        this.player = player;
        this.scores = new Integer[MAX_SHOTS];
        this.location = location.clone();
        // Set location to the exact middle of the block
        this.location.setX(location.getBlockX() + 0.5);
        this.location.setZ(location.getBlockZ() + 0.5);
        this.location.add(0.49,0.5,0);
    }

    public static Collection<ArcheryScore> getAll(){
        return players.values();
    }

    public boolean addScore(int addScore){
        for(int i = 0; i < scores.length; i++){
            if(scores[i] == null){
                scores[i] = addScore;
                return true;
            }
        }
        return false;
    }

    public int getScore(){
        int total = 0;
        for(Integer i : scores){
            if(i != null)
                total += i;
        }
        return total;
    }

    public Player getPlayer(){
        return player;
    }

    public Location getLocation(){
        return location;
    }

    public static Optional<ArcheryScore> getArcheryScore(Player player){
        return Optional.ofNullable(players.get(player));
    }

    public static void clear(){
        players.clear();
    }

}
