package com.orangemarshall.olympics.disciplines.archery;

import com.orangemarshall.olympics.Main;
import com.orangemarshall.olympics.disciplines.Discipline;
import com.orangemarshall.olympics.listeners.MovePreventionListener;
import com.orangemarshall.olympics.utils.Meta;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;


public class ArcheryDiscipline extends Discipline{

    private Location base_location;

    public ArcheryDiscipline(Main plugin, String... args){
        super(plugin, args);
    }

    @Override
    public String getName(){
        return "Archery";
    }

    @Override
    public String getInfo(){
        return "   Hit the middle of the target as\n" +
               "      as accurately as possible!\n" +
               "    You can only score five times!";
    }

    @Override
    public void loadListeners(){
        listeners.add(new ArrowHitTargetListener());
        listeners.add(new MovePreventionListener(true));
        countdownListeners.add(new MovePreventionListener(true));
    }

    @Override
    public void startCountdown(){
        super.startCountdown();
        loadConfig();
        prepareMap();
    }

    @Override
    public void end(){
        super.end();
        clearMap();
    }

    private void loadConfig(){
        String[] spl = plugin.getConfig().getString(args[0]).split(",");
        base_location = new Location(Bukkit.getWorld(spl[0]), Integer.parseInt(spl[1]), Integer.parseInt(spl[2]), Integer.parseInt(spl[3]));
        base_location.setDirection(new Vector(-1, 0, 0)); // West
        base_location.add(0, 0, 0.5); // Location where people will startDiscipline spawning in a row (and according targets are built)

    }

    private void prepareMap(){
        Location location = base_location.clone();

        // Random distance for target
        int distance = ThreadLocalRandom.current().nextInt(ArcheryScore.MIN_DISTANCE, ArcheryScore.MAX_DISTANCE + 1);

        // Spawn players and build their according target
        for(Player player : Bukkit.getOnlinePlayers()){
            clearShootingRange(location);
            player.teleport(location);
            //TODO tp to middle of block
            Location target_location = location.clone().add(-distance,1,0);
            new ArcheryScore(player, target_location);
            buildTarget(target_location);
            location.add(0,0,-7);//TODO test
        }
    }

    // Builds the target at given location (middle)
    private void buildTarget(Location location){
        Location loc = location.clone();
        Material material = Material.WOOL;
        loc.getBlock().setType(material);
        loc.getBlock().setData((byte) 14); // Red wool
        loc.getBlock().setMetadata(Meta.ARCHERY_TARGET, new FixedMetadataValue(plugin, true));
        loc.add(0, 1, -1).getBlock().setType(material);
        loc.getBlock().setMetadata(Meta.ARCHERY_TARGET, new FixedMetadataValue(plugin, true));
        loc.add(0, 0, 1).getBlock().setType(material);
        loc.getBlock().setMetadata(Meta.ARCHERY_TARGET, new FixedMetadataValue(plugin, true));
        loc.add(0, 0, 1).getBlock().setType(material);
        loc.getBlock().setMetadata(Meta.ARCHERY_TARGET, new FixedMetadataValue(plugin, true));
        loc.add(0, -1, 0).getBlock().setType(material);
        loc.getBlock().setMetadata(Meta.ARCHERY_TARGET, new FixedMetadataValue(plugin, true));
        loc.add(0, -1, 0).getBlock().setType(material);
        loc.getBlock().setMetadata(Meta.ARCHERY_TARGET, new FixedMetadataValue(plugin, true));
        loc.add(0, 0, -1).getBlock().setType(material);
        loc.getBlock().setMetadata(Meta.ARCHERY_TARGET, new FixedMetadataValue(plugin, true));
        loc.add(0, 0, -1).getBlock().setType(material);
        loc.getBlock().setMetadata(Meta.ARCHERY_TARGET, new FixedMetadataValue(plugin, true));
        loc.add(0, 1, 0).getBlock().setType(material);
        loc.getBlock().setMetadata(Meta.ARCHERY_TARGET, new FixedMetadataValue(plugin, true));
    }

    // Removes the target at given location (middle)
    private void clearTarget(Location location){
        Location loc = location.clone();
        Material material = Material.AIR;
        loc.getBlock().setType(material);
        loc.add(0, 1, -1).getBlock().setType(material);
        loc.add(0, 0, 1).getBlock().setType(material);
        loc.add(0, 0, 1).getBlock().setType(material);
        loc.add(0, -1, 0).getBlock().setType(material);
        loc.add(0, -1, 0).getBlock().setType(material);
        loc.add(0, 0, -1).getBlock().setType(material);
        loc.add(0, 0, -1).getBlock().setType(material);
        loc.add(0, 1, 0).getBlock().setType(material);
    }

    private void clearShootingRange(Location location){
        Location loc = location.clone().add(0,1,0);
        for(int i = 0; i < ArcheryScore.MAX_DISTANCE; i++){
            clearTarget(loc.add(-1,0,0));
        }
    }

    //Removes remaining arrows and blocks
    private void clearMap(){
        ArcheryScore.getAll().forEach(score -> clearTarget(score.getLocation()));
        Bukkit.getWorld("world").getEntitiesByClass(Arrow.class).forEach(Arrow::remove);
    }

    @Override
    public String getScoreboardLine1(Player player){
        return "Current score:";
    }

    @Override
    public String getScoreboardLine2(Player player){
        Optional<ArcheryScore> score = ArcheryScore.getArcheryScore(player);

        if(score.isPresent()){
            return ChatColor.GOLD.toString() + score.get().getScore();
        }else{
            return "Unavailable";
        }
    }

    @Override
    public void addGlobalScore(){
        int i = 3;

        Collection<ArcheryScore> all = ArcheryScore.getAll();

        List<Player> noScore = all.stream()
                        .filter(entry -> entry.getScore() == 0)
                        .map(ArcheryScore::getPlayer)
                        .collect(Collectors.toList());

        List<Player> entries = ArcheryScore.getAll().stream()
                        .filter(entry -> entry.getScore() != 0)
                        .sorted((entry1, entry2) -> Integer.compare(entry2.getScore(), entry1.getScore()))
                        .limit(3)
                        .map(ArcheryScore::getPlayer)
                        .collect(Collectors.toList());

        for(Player player : entries){
            addGlobalScore(player, i--);
        }

        for(Player player : noScore){
            player.sendMessage(ChatColor.RED + "You didn't score, so you will not be rewarded for this game!");
        }
    }

    @Override
    public void preparePlayer(Player player){
        super.preparePlayer(player);

        ItemStack bow = new ItemStack(Material.BOW);
        bow.addEnchantment(Enchantment.ARROW_INFINITE, 1);

        player.getInventory().setItem(0, bow);
        player.getInventory().setItem(8, new ItemStack(Material.ARROW));
    }
}
