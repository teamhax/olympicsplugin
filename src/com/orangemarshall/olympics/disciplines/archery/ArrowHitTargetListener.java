package com.orangemarshall.olympics.disciplines.archery;

import com.orangemarshall.olympics.Main;
import com.orangemarshall.olympics.utils.Meta;
import com.orangemarshall.olympics.utils.Notifier;
import com.orangemarshall.olympics.utils.ReflectionUtils;
import net.minecraft.server.v1_8_R3.EntityArrow;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftArrow;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.scheduler.BukkitRunnable;

/*
    Registers arrow hits from a player on their target.
 */

public class ArrowHitTargetListener implements Listener{

    @EventHandler
    public void onHit(ProjectileHitEvent event){
        Projectile projectile = event.getEntity();
        if(projectile.getShooter() instanceof Player && projectile instanceof Arrow){
            new ArrowProcessor((Arrow) projectile, (Player) projectile.getShooter());
        }
    }
}

class ArrowProcessor extends BukkitRunnable{

    private Arrow arrow;
    private Player player;

    // Processes the arrow one tick later, so the arrow contains the block it's stuck in.
    public ArrowProcessor(Arrow arrow, Player player){
        this.arrow = arrow;
        this.player = player;
        this.runTaskLater(Main.instance(), 1);
    }

    @Override
    public void run(){
        Block block = getBlockFromArrow(arrow);
        if(block.hasMetadata(Meta.ARCHERY_TARGET)){
            ArcheryScore.getArcheryScore(player).ifPresent((archeryScore) -> {
                double distanceArrowToMiddle = Math.abs(archeryScore.getLocation().distance(arrow.getLocation()));
                int score = calculateScore(distanceArrowToMiddle);

                if(archeryScore.addScore(score)){
                    String message = String.format(ChatColor.GOLD.toString() + ChatColor.BOLD + "+%d Points " + ChatColor.YELLOW + "(Distance: %.2f)", score, distanceArrowToMiddle);
                    Notifier.newNotifier().toPlayer(archeryScore.getPlayer()).withSound(Sound.ORB_PICKUP).send(message);
                }else{
                    String message = String.format(ChatColor.RED + "You already scored enough!");
                    Notifier.newNotifier().toPlayer(archeryScore.getPlayer()).send(message);
                }
            });
        }
    }

    private Block getBlockFromArrow(Arrow arrow){
        EntityArrow arrowNMS = ((CraftArrow)arrow).getHandle();
        int x = ReflectionUtils.get("d", EntityArrow.class, arrowNMS);
        int y = ReflectionUtils.get("e", EntityArrow.class, arrowNMS);
        int z = ReflectionUtils.get("f", EntityArrow.class, arrowNMS);
        Location location = new Location(arrow.getWorld(), x, y, z);
        return arrow.getWorld().getBlockAt(location);
    }

    // Calculate score for the shot based on distance to the middle of the target.
    private int calculateScore(double distance){
        if(distance > 2.0){
            return 0;
        }else{
            return (int) Math.max(0, Math.round(100 - 50*distance));
        }
    }

}