package com.orangemarshall.olympics.disciplines;

import org.bukkit.entity.Player;


public interface IDiscipline{

    void startCountdown();
    void end();
    void startDiscipline();
    boolean isCountingDown();
    String getName();
    String getInfo();
    int getDuration();
    String getScoreboardLine1(Player player);
    String getScoreboardLine2(Player player);
    void addGlobalScore();

}
