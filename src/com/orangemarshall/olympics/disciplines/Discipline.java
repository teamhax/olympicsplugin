package com.orangemarshall.olympics.disciplines;

import com.orangemarshall.olympics.Main;
import com.orangemarshall.olympics.utils.Countdown;
import com.orangemarshall.olympics.utils.GameNotifier;
import com.orangemarshall.olympics.utils.GlobalPlayerScore;
import com.orangemarshall.olympics.utils.Notifier;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/*
    Base for disciplines, handling of listeners provided.
    Default duration of 30 seconds until discipline is over.
 */

public abstract class Discipline implements IDiscipline{

    protected Main plugin;
    protected List<Listener> listeners;
    protected List<Listener> countdownListeners;
    protected String[] args;
    protected Countdown countdown = null;

    public Discipline(Main plugin, String... args){
        this.plugin = plugin;
        this.args = args;
        this.countdownListeners = new ArrayList<>();
        this.listeners = new ArrayList<>();
        loadListeners();
    }

    @Override
    public void startCountdown(){
        countdown = new Countdown(plugin, this::startDiscipline, countdownListeners);

        GameNotifier notifier = GameNotifier.newNotifier(getName());
        for(String line : getInfo().split("\n")){
            notifier.addLine("          " + ChatColor.YELLOW.toString() + ChatColor.BOLD + line);
        }
        notifier.print();

        preparePlayers();
    }

    @Override
    public void startDiscipline(){
        countdown = null;
        registerListeners();
    }

    @Override
    public boolean isCountingDown(){
        return countdown != null;
    }

    @Override
    public void end(){
        addGlobalScore();
        unregisterListeners();
    }

    protected void registerListeners(){
        listeners.forEach(plugin::register);
    }

    protected void unregisterListeners(){
        listeners.forEach(plugin::unregister);
    }

    @Override
    public int getDuration(){
        return 20 * 30;
    }

    @Override
    public String getScoreboardLine1(Player player){
        return "";
    }

    @Override
    public String getScoreboardLine2(Player player){
        return "";
    }

    protected void preparePlayers(){
        Bukkit.getOnlinePlayers().forEach(this::preparePlayer);
    }

    protected void preparePlayer(Player player){
        player.closeInventory();
        player.getInventory().clear();
        player.getInventory().setArmorContents(new ItemStack[]{null, null, null, null});
        player.getActivePotionEffects().forEach(pe -> player.removePotionEffect(pe.getType()));
        player.setFlying(false);
        player.setLevel(0);
        player.setExp(0);
        player.setHealth(player.getMaxHealth());
        player.setFoodLevel(20);
        player.setWalkSpeed(0.2f);
    }

    protected void addGlobalScore(Player player, int score){
        String magic = ChatColor.GOLD.toString() + ChatColor.MAGIC + "MM" + ChatColor.RESET;
        String scoreString = ChatColor.GOLD.toString() + ChatColor.BOLD + score;
        String plural = ((score > 1) ? "s" : "");
        Notifier.newNotifier()
                .toPlayer(player)
                .withSound(Sound.LEVEL_UP)
                .send(magic + ChatColor.YELLOW + " You received " + scoreString + ChatColor.YELLOW + " star" + plural + " from the " + ChatColor.GREEN + getName() + ChatColor.YELLOW + " discipline! " + magic);
        GlobalPlayerScore.getPlayerScore(player).addScore(score);
    }

    protected abstract void loadListeners();

}
