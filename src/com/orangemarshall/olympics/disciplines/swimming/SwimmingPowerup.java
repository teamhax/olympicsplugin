package com.orangemarshall.olympics.disciplines.swimming;

import com.orangemarshall.olympics.Main;
import com.orangemarshall.olympics.pickableitems.PickableItem;
import com.orangemarshall.olympics.utils.Notifier;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.concurrent.ThreadLocalRandom;

/*
    Spawns a floating powerup, which has a value (and colors & names depending on value).
 */

public class SwimmingPowerup extends PickableItem{

    private Levels level;

    public SwimmingPowerup(Location location, Levels level){
        super(location, level.getItemStack());
        this.setName(level.getColor() + ChatColor.BOLD.toString() + "+" + level.getLevel() + " Point" + ((level.getLevel() > 1) ? "s" : ""));
        this.level = level;
    }

    @Override
    public void onPickup(Player player){
        super.onPickup(player);
        int score = level.getLevel();
        String message = level.getColor().toString() + ChatColor.BOLD + "+" + score + " Point" + ((score > 1) ? "s" : "");
        Notifier.newNotifier().toPlayer(player).withSound(Sound.ORB_PICKUP).send(message);
        SwimmingDiscipline discipline = ((SwimmingDiscipline)Main.instance().getCurrentDiscipline());
        discipline.getSpawner().spawnItem();
        discipline.addScore(player, score);
    }

    enum Levels{

        LEVEL_1(10, ChatColor.RED, new ItemStack(Material.WOOL, 0, (short)0, (byte)14)),
        LEVEL_2(20, ChatColor.GOLD, new ItemStack(Material.WOOL, 0, (short)0, (byte)1)),
        LEVEL_3(30, ChatColor.GREEN, new ItemStack(Material.WOOL, 0, (short)0, (byte)5));

        private int level;
        private ChatColor color;
        private ItemStack itemStack;

        Levels(int level, ChatColor color, ItemStack itemStack){
            this.level = level;
            this.color = color;
            this.itemStack = itemStack;
        }

        public ChatColor getColor(){
            return color;
        }

        public int getLevel(){
            return level;
        }

        public ItemStack getItemStack(){
            return itemStack;
        }

        public static Levels getRandom(){
            int random = ThreadLocalRandom.current().nextInt(0, Levels.values().length);
            return Levels.values()[random];
        }

    }

}
