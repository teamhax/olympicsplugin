package com.orangemarshall.olympics.disciplines.swimming;

import com.orangemarshall.olympics.Main;
import com.orangemarshall.olympics.disciplines.Discipline;
import com.orangemarshall.olympics.listeners.MovePreventionListener;
import com.orangemarshall.olympics.pickableitems.PickableItemListener;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class SwimmingDiscipline extends Discipline{

    private SwimmingPowerupSpawner spawner;
    private BasicScore score;
    private int playerSpawnY;

    public SwimmingDiscipline(Main plugin, String... args){
        super(plugin, args);
        score = new BasicScore();
    }

    @Override
    public String getName(){
        return "Swimming";
    }

    @Override
    public String getInfo(){
        return "     Jump out of the water like a\n" +
               "  dolphin and try to earn most points!";
    }

    @Override
    public void loadListeners(){
        listeners.add(new SwimmingListener());
        countdownListeners.add(new MovePreventionListener(true));
    }

    public void addScore(Player player, int score){
        this.score.addScore(player, score);
    }

    @Override
    public String getScoreboardLine1(Player player){
        return "Current score:";
    }

    @Override
    public String getScoreboardLine2(Player player){
        return ChatColor.GOLD.toString() + score.getScore(player);
    }

    @Override
    public void startCountdown(){
        super.startCountdown();
        loadConfig();
        spawnPlayers();
        spawner.spawnItem(10);
    }

    @Override
    public void end(){
        super.end();
        //TODO Add to main listeners or unregister properly
        PickableItemListener.unregisterAll();
    }

    private void loadConfig(){
        String[] spl_low = plugin.getConfig().getString(args[0]).split(",");
        String[] spl_high = plugin.getConfig().getString(args[1]).split(",");
        spawner = new SwimmingPowerupSpawner(
                new Location(Bukkit.getWorld(spl_low[0]), Integer.parseInt(spl_low[1]), Integer.parseInt(spl_low[2]), Integer.parseInt(spl_low[3])),
                new Location(Bukkit.getWorld(spl_high[0]), Integer.parseInt(spl_high[1]), Integer.parseInt(spl_high[2]), Integer.parseInt(spl_high[3])));
        playerSpawnY = Integer.parseInt(spl_low[2]) - 2;
    }

    private void spawnPlayers(){
        Bukkit.getOnlinePlayers().forEach(this::spawnPlayer);
    }

    private void spawnPlayer(Player player){
        Location location = player.getLocation();
        location.setX(spawner.getRandomX());
        location.setY(playerSpawnY);
        location.setZ(spawner.getRandomZ());
        player.teleport(location);
    }

    @Override   // Give 3 to first, 2 to second, 1 to third
    public void addGlobalScore(){
        int i = 3;

        Set<Map.Entry<Player,Integer>> all = score.getAll();

        List<Player> noScore = all.stream()
                .filter(entry -> entry.getValue() == 0)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        List<Player> entries = score.getAll().stream()
                .filter(entry -> entry.getValue() != 0)
                .sorted((entry1, entry2) -> Integer.compare(entry2.getValue(), entry1.getValue()))
                .limit(3)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        for(Player player : entries){
            addGlobalScore(player, i--);
        }

        for(Player player : noScore){
            player.sendMessage(ChatColor.RED + "You didn't score, so you will not be rewarded for this game!");
        }
    }

    public SwimmingPowerupSpawner getSpawner(){
        return spawner;
    }
}
