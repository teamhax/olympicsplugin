package com.orangemarshall.olympics.disciplines.swimming;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.concurrent.ThreadLocalRandom;

/*
    Stores the bounding box for powerups to spawn in.
    Spawns powerups at a random position within it.
 */

public class SwimmingPowerupSpawner{

    private Location location_low, location_high;

    public SwimmingPowerupSpawner(Location location_low, Location location_high){
        this.location_low = location_low;
        this.location_high = location_high;
    }

    public void spawnItem(int amount){
        while(amount > 0){
            spawnItem();
            amount--;
        }
    }

    public void spawnItem(){
        Location location = new Location(Bukkit.getWorld("world"), getRandomX(), getRandomY(), getRandomZ());
        new SwimmingPowerup(location, SwimmingPowerup.Levels.getRandom());
    }

    public int getRandomX(){
        return ThreadLocalRandom.current().nextInt(location_low.getBlockX(), location_high.getBlockX() + 1);
    }

    public int getRandomY(){
        return ThreadLocalRandom.current().nextInt(location_low.getBlockY(), location_high.getBlockY() + 1);
    }

    public int getRandomZ(){
        return ThreadLocalRandom.current().nextInt(location_low.getBlockZ(), location_high.getBlockZ() + 1);
    }

}
