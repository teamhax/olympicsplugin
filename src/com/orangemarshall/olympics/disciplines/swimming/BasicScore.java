package com.orangemarshall.olympics.disciplines.swimming;

import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/*
    Basic player -> score
 */

public class BasicScore{

    private final HashMap<Player, Integer> players = new HashMap<>();

    public BasicScore(){}

    public void addScore(Player player, int score){
        players.put(player, getScore(player) + score);
    }

    public int getScore(Player player){
        Integer score = players.get(player);
        if(score == null){
            players.put(player, 0);
            score = 0;
        }
        return score;
    }

    public Set<Map.Entry<Player, Integer>> getAll(){
        return players.entrySet();
    }

}
