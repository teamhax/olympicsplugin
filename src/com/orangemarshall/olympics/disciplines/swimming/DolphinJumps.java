package com.orangemarshall.olympics.disciplines.swimming;

import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.HashMap;

/*
    Called when a player is supposed to get a movement boost.
 */

public class DolphinJumps{

    private final static long COOLDOWN = 1000 * 2;

    private HashMap<Player,Long> lastJumpTime = new HashMap<>();

    public void attemptJump(Player player){
        Long lastTime = lastJumpTime.get(player);
        if(lastTime == null){
            jump(player);
            lastJumpTime.put(player, System.currentTimeMillis());
        }else if(System.currentTimeMillis() - lastTime > COOLDOWN){
            jump(player);
            lastJumpTime.put(player, System.currentTimeMillis());
        }
    }

    public void jump(Player player){
        Vector vector = player.getEyeLocation().getDirection().normalize();
        if(vector.getY() > 0.0){
            player.setVelocity(player.getVelocity().add(vector.multiply(2.5)));
        }
    }

}
