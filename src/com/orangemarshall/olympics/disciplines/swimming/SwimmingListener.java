package com.orangemarshall.olympics.disciplines.swimming;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/*
    Listens to the player moving out of water and attempts to give him a movement boost.
 */

public class SwimmingListener implements Listener{

    private DolphinJumps dolphinJumps;

    public SwimmingListener(){
        dolphinJumps = new DolphinJumps();
    }

    @EventHandler
    public void onJumpOutOfWater(PlayerMoveEvent event){
        if(isJustOutOfWater(event.getTo())){
            if(isMovingUp(event.getFrom(), event.getTo())){
                dolphinJumps.attemptJump(event.getPlayer());
            }
        }
    }

    private boolean isJustOutOfWater(Location location){
        Location loc = location.clone();
        if(loc.getBlock().getType() == Material.STATIONARY_WATER){
            if(loc.add(0,1,0).getBlock().getType() == Material.AIR){
                return true;
            }
        }
        return false;
    }

    private boolean isMovingUp(Location from, Location to){
        return to.getY() > from.getY();
    }
}
