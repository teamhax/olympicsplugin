package com.orangemarshall.olympics.disciplines;

import com.orangemarshall.olympics.Main;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;


public class CommandExecutor implements org.bukkit.command.CommandExecutor{

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] strings){
        if(label.equalsIgnoreCase("startolympics")){
            if(commandSender.isOp()){
                if(Main.instance().startOlympics()){
                    commandSender.sendMessage(ChatColor.GREEN + "You have started the Olympics!");
                }else{
                    commandSender.sendMessage(ChatColor.RED + "They have already started!");
                }
            }else{
                commandSender.sendMessage(ChatColor.RED + "No permission!");
            }
        }

        return false;
    }
}
