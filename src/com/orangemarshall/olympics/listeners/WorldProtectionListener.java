package com.orangemarshall.olympics.listeners;

import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.EntityExplodeEvent;

public class WorldProtectionListener implements Listener{

    @EventHandler
    public void onFire(BlockBurnEvent event){
        event.setCancelled(true);
    }

    @EventHandler
    public void onDamage(BlockDamageEvent event){
        event.setCancelled(true);
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent event){
        event.setCancelled(event.getPlayer().getGameMode() != GameMode.CREATIVE);
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event){
        event.setCancelled(event.getPlayer().getGameMode() != GameMode.CREATIVE);
    }

    @EventHandler
    public void onBlockExplode(EntityExplodeEvent event){
        event.setCancelled(true);
    }


}
