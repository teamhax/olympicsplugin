package com.orangemarshall.olympics.listeners;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class DisableSuicideListener implements Listener{

    @EventHandler
    public void onSlashKill(PlayerCommandPreprocessEvent event){
        if(event.getMessage().startsWith("/kill") || event.getMessage().startsWith("/minecraft:kill")){
            event.setCancelled(true);
            event.getPlayer().sendMessage(ChatColor.RED + "Suicide is never an option.");
        }
    }

}
