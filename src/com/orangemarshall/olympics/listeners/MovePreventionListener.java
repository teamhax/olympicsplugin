package com.orangemarshall.olympics.listeners;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/*
    Prevents players to move.
 */

public class MovePreventionListener implements Listener{

    private boolean allowMovementOnCurrentBlock;

    public MovePreventionListener(boolean allowMovementOnCurrentBlock){
        this.allowMovementOnCurrentBlock = allowMovementOnCurrentBlock;
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event){
        Location locationFrom = event.getFrom();
        Location locationTo = event.getTo();

        if(!allowMovementOnCurrentBlock || hasMoved(locationFrom, locationTo)){
           setBack(event.getPlayer(), locationFrom, locationTo);
        }
    }

    private boolean hasMoved(Location from, Location to){
        return from.getBlockX() != to.getBlockX() || from.getBlockZ() != to.getBlockZ();
    }

    private void setBack(Player player, Location from, Location to){
        from.setYaw(to.getYaw());
        from.setPitch(to.getPitch());
        from.setY(to.getY());
        player.teleport(from);
    }

}
