package com.orangemarshall.olympics.listeners;

import net.minecraft.server.v1_8_R3.AxisAlignedBB;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;


public class PlayerEntersAreaListener implements Listener{

    private AxisAlignedBB boundingBox;
    private Consumer<Player> consumer;
    private List<Player> alreadyTriggered;

    public PlayerEntersAreaListener(Consumer<Player> consumer, double x1, double y1, double z1, double x2, double y2, double z2){
        this(consumer, new AxisAlignedBB(x1, y1, z1, x2, y2, z2));
    }

    public PlayerEntersAreaListener(Consumer<Player> consumer, Location location1, Location location2){
        this(consumer, new AxisAlignedBB(location1.getX(), location1.getY(), location1.getZ(), location2.getX(), location2.getY(), location2.getZ()));
    }

    public PlayerEntersAreaListener(Consumer<Player> consumer, AxisAlignedBB boundingBox){
        this.boundingBox = boundingBox;
        this.consumer = consumer;
        this.alreadyTriggered = new ArrayList<>();
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event){
        Player player = event.getPlayer();
        boolean flag = ((CraftPlayer)player).getHandle().getBoundingBox().b(boundingBox);
        if(flag && !alreadyTriggered.contains(player)){
            consumer.accept(player);
            alreadyTriggered.add(player);
        }
    }

}
