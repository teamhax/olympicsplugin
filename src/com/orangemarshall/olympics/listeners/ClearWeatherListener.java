package com.orangemarshall.olympics.listeners;

import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;

public class ClearWeatherListener implements Listener{

    public ClearWeatherListener(World world){
        world.setThundering(false);
        world.setStorm(false);
    }

    @EventHandler
    public void onWeatherChange(WeatherChangeEvent event){
        event.setCancelled(true);
    }

}
