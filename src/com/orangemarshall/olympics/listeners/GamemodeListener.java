package com.orangemarshall.olympics.listeners;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class GamemodeListener implements Listener{

    private GameMode gameMode;

    public GamemodeListener(GameMode gameMode){
        this.gameMode = gameMode;
        Bukkit.getOnlinePlayers().forEach(player -> player.setGameMode(gameMode));
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event){
        event.getPlayer().setGameMode(gameMode);
    }

}
