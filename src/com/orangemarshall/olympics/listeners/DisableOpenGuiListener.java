package com.orangemarshall.olympics.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryOpenEvent;

public class DisableOpenGuiListener implements Listener{

    @EventHandler
    public void onOpen(InventoryOpenEvent event){
        event.setCancelled(true);
    }

}
