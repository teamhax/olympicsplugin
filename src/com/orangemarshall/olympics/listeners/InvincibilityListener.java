package com.orangemarshall.olympics.listeners;

import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class InvincibilityListener implements Listener{

    public InvincibilityListener(World world){
        world.setPVP(false);
    }

    @EventHandler
    public void onDamage(EntityDamageEvent event){
        event.setCancelled(true);
    }
}
