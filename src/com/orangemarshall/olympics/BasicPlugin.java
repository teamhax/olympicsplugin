package com.orangemarshall.olympics;

import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

/*
    Base for the BasicPlugin class
 */

public abstract class BasicPlugin extends JavaPlugin{

    public void register(Listener listener){
        Bukkit.getPluginManager().registerEvents(listener, this);
    }

    public void unregister(Listener listener){
        HandlerList.unregisterAll(listener);
    }

}
