package com.orangemarshall.olympics.utils;

import org.bukkit.entity.Player;

import java.util.*;

/*
    Unused
    Will be used to store each player's score throughout the olympics.
 */

public class GlobalPlayerScore{

    private static final HashMap<Player, GlobalPlayerScore> players = new HashMap<>();

    private Player player;
    private int score;

    private GlobalPlayerScore(Player player){
        players.put(player, this);
        this.player = player;
    }

    public int addScore(int score){
        return (this.score += score);
    }

    public Player getPlayer(){
        return player;
    }

    public int getScore(){
        return score;
    }

    public static GlobalPlayerScore getPlayerScore(Player player){
        GlobalPlayerScore globalPlayerScore = players.get(player);
        if(globalPlayerScore == null){
            globalPlayerScore = new GlobalPlayerScore(player);
        }
        return globalPlayerScore;
    }

    public static Set<Map.Entry<Player, GlobalPlayerScore>> getAll(){
        return players.entrySet();
    }

    public static void clear(){
        players.clear();
    }

}
