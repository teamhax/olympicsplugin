package com.orangemarshall.olympics.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/*
    Sends messages to one or all players, with an optional sound.
 */

public class Notifier{

    private Player player = null;
    private Sound sound = null;
    private Location soundLocation = null;
    private String message = null;

    private Notifier(){

    }

    public static Notifier newNotifier(){
        return new Notifier();
    }

    public Notifier toEveryone(){
        player = null;
        return this;
    }

    public Notifier toPlayer(Player player){
        this.player = player;
        return this;
    }

    public Notifier withSound(Sound sound){
        this.sound = sound;
        this.soundLocation = null;
        return this;
    }

    public Notifier withSoundAt(Sound sound, Location soundLocation){
        this.sound = sound;
        this.soundLocation = soundLocation;
        return this;
    }

    public void send(String message){
        if(message == null)
            throw new NullPointerException("Message cannot be null!");

        this.message = message;
        if(player == null){
            Bukkit.getOnlinePlayers().forEach(this::send);
        }else{
            send(player);
        }
    }

    private void send(Player player){
        player.sendMessage(message);

        if(sound != null){
            if(soundLocation == null){
                soundLocation = player.getLocation();
            }
            player.playSound(soundLocation, sound, 1.0f, 1.0f);
        }
    }

}
