package com.orangemarshall.olympics.utils;

import com.orangemarshall.olympics.Main;
import com.orangemarshall.olympics.disciplines.IDiscipline;
import com.orangemarshall.olympics.disciplines.ITickEvent;
import org.bukkit.scheduler.BukkitRunnable;

/*
    Used to switch discipline based on time and also display remaining time.
 */

public class TickRunnable extends BukkitRunnable{

    private int totalTicks = 0;
    private int gameTicks = 0;

    public void start(){
        this.runTaskTimer(Main.instance(), 1, 1);
    }

    public int getTimeRemaining(){
        return (Main.instance().getCurrentDiscipline().getDuration() - gameTicks) / 20;
    }

    @Override
    public void run(){
        IDiscipline discipline = Main.instance().getCurrentDiscipline();
        if(!discipline.isCountingDown()){
            if(discipline.getDuration() < gameTicks){
                Main.instance().nextDiscipline();
                this.cancel();
            }else{
                if(discipline instanceof ITickEvent){
                    ((ITickEvent) discipline).onTick(gameTicks);
                }
            }
            gameTicks++;
        }
        totalTicks++;
    }

}
