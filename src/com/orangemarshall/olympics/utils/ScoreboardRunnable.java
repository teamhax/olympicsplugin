package com.orangemarshall.olympics.utils;

import com.orangemarshall.olympics.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

public class ScoreboardRunnable extends BukkitRunnable{

    private TickRunnable runnable;

    public ScoreboardRunnable(TickRunnable runnable){
        this.runnable = runnable;
    }

    public void start(){
        this.runTaskTimer(Main.instance(), 20, 10);
    }

    @Override
    public void run(){
        Bukkit.getOnlinePlayers().forEach(this::updateScoreboard);
    }

    private void updateScoreboard(Player player){
        Scoreboard scoreboard = new ScoreboardBuilder(ChatColor.YELLOW.toString() + ChatColor.BOLD + Main.instance().getGameName())
                .setLine(14, ChatColor.AQUA + player.getName())
                .setLine(12, ChatColor.WHITE.toString() + "Info: ")
                .setLine(11, ChatColor.YELLOW + Main.instance().getCurrentDiscipline().getName())
                .setLine(9, ChatColor.WHITE.toString() + "Time remaining:")
                .setLine(8, ChatColor.RED + formatSeconds(runnable.getTimeRemaining()))
                .setLine(6, Main.instance().getCurrentDiscipline().getScoreboardLine1(player))
                .setLine(5, Main.instance().getCurrentDiscipline().getScoreboardLine2(player))
                .setLine(3, ChatColor.WHITE + "Total stars: " + ChatColor.GOLD.toString() + ChatColor.BOLD + GlobalPlayerScore.getPlayerScore(player).getScore())  //TODO change to total score
                .setLine(1, ChatColor.YELLOW + "www.foundation.tf")
                .build();

        player.setScoreboard(scoreboard);
    }

    private String formatSeconds(int seconds) {
        return String.format("%ds", seconds);
    }


    public static class ScoreboardBuilder {

        private Scoreboard scoreboard;
        private Objective objective;
        private boolean[] set = new boolean[16];

        public ScoreboardBuilder(String title) {
            scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
            objective = scoreboard.registerNewObjective("title", "dummy");
            objective.setDisplaySlot(DisplaySlot.SIDEBAR);
            objective.setDisplayName(title);
        }

        public ScoreboardBuilder(String title, Scoreboard scoreboard) {
            this.scoreboard = scoreboard;
            scoreboard.getEntries().forEach(e -> scoreboard.resetScores(e));
            Objective obj = this.scoreboard.getObjective("title");
            if (obj != null) {
                obj.setDisplayName(title);
                objective = obj;
            } else {
                objective = scoreboard.registerNewObjective("title", "dummy");
                objective.setDisplaySlot(DisplaySlot.SIDEBAR);
                objective.setDisplayName(title);
            }
        }

        public boolean isBuilt() {
            return scoreboard != null && objective != null;
        }

        public Scoreboard build() {
            for (int i = 0; i < set.length; i++) {
                if (!set[i]) {
                    Score score = objective.getScore(ChatColor.COLOR_CHAR + Integer.toHexString(i));
                    score.setScore(i);
                }
            }

            return isBuilt() ? scoreboard : null;
        }

        public ScoreboardBuilder setLine(int row, String line) {
            set[row] = true;
            objective.getScore(line).setScore(row);
            return this;
        }

    }

}
