package com.orangemarshall.olympics.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.List;

public class GameNotifier{

    private List<String> lines;
    private String name;

    private GameNotifier(String name){
        this.name = name;
        lines = new ArrayList<>();
    }

    public static GameNotifier newNotifier(String name){
        return new GameNotifier(name);
    }

    public GameNotifier addLine(String line){
        lines.add(line);
        return this;
    }

    public void print(){
        lines.add(0, ChatColor.GREEN + new String(new char[64]).replace("\0", "\u25AC"));
        lines.add(1, "                                      " + ChatColor.BOLD + name);
        lines.add(2, "");
        lines.add("");
        lines.add(ChatColor.GREEN + new String(new char[64]).replace("\0", "\u25AC"));
        lines.forEach(Bukkit::broadcastMessage);
    }

}
