package com.orangemarshall.olympics.utils;

import com.orangemarshall.olympics.Main;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.scheduler.BukkitRunnable;


public class FireworkSpawner{

    public static void spawnRandomFireworks(Player player, int amount) {
        if (player == null || player.getLocation() == null || amount <= 0)
            return;

        spawnRandomFirework(player.getEyeLocation());
        final int i = --amount;

        new BukkitRunnable() {
            @Override
            public void run() {
                spawnRandomFireworks(player, i);
            }
        }.runTaskLater(Main.instance(), 20 * (int) (Math.random() * 2 + 0.5));
    }

    public static Firework spawnRandomFirework(Location loc) {
        Firework fw = (Firework) loc.getWorld().spawnEntity(loc, EntityType.FIREWORK);
        FireworkMeta fwm = fw.getFireworkMeta();
        FireworkEffect.Type type = FireworkEffect.Type.values()[(int) (FireworkEffect.Type.values().length * Math.random())];
        FireworkEffect effect = FireworkEffect.builder().flicker(Math.random() > 0.5)
                .withColor(getRandomColor()).withFade(getRandomColor()).with(type)
                .trail(Math.random() > 0.5).build();
        fwm.addEffect(effect);
        fwm.setPower((int) (Math.random() * 2 + 1));
        fw.setFireworkMeta(fwm);

        return fw;
    }

    public static Color getRandomColor() {
        return Color.fromRGB((int) (Math.random() * 256), (int) (Math.random() * 256), (int) (Math.random() * 256));
    }

}
