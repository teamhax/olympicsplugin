package com.orangemarshall.olympics.utils;

import com.orangemarshall.olympics.Main;
import com.orangemarshall.olympics.listeners.MovePreventionListener;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;


public class Countdown{
    private int duration;
    private int totalTicks;
    private Main plugin;
    private BukkitRunnable runnable;
    private MovePreventionListener listener;
    private Runnable onFinish;
    private List<Listener> listeners;

    public Countdown(Main plugin, Runnable onFinish, List<Listener> listeners){
        this(plugin, 20 * 10, onFinish, listeners);
    }

    public Countdown(Main plugin, int duration, Runnable onFinish, List<Listener> listeners){
        this.plugin = plugin;
        this.duration = duration;//TODO movement listener
        this.onFinish = onFinish;
        this.listeners = listeners;

        registerListeners();

        runnable = new BukkitRunnable(){
            @Override
            public void run(){
                onTick();
            }
        };
        runnable.runTaskTimer(plugin, 1, 1);
    }

    private void registerListeners(){
        if(listeners != null){
            for(Listener listener : listeners){
                plugin.register(listener);
            }
        }
    }

    private void unregisterListeners(){
        if(listeners != null){
            for(Listener listener : listeners){
                plugin.unregister(listener);
            }
        }
    }

    public int getDuration(){
        return duration;
    }

    public void onTick(){
        if(totalTicks % 20 == 0){
            sendCountdown((duration - totalTicks) / 20);
        }
        if(totalTicks - duration >= 0){
            onFinish();
        }
        totalTicks++;
    }

    private void onFinish(){
        runnable.cancel();
        unregisterListeners();
        onFinish.run();
    }

    private void sendCountdown(int seconds){
        String name = plugin.getCurrentDiscipline().getName();

        if(seconds == 0){
            String message = ChatColor.GREEN.toString() + ChatColor.BOLD + name + ChatColor.GREEN + " has started!";
            Notifier.newNotifier().withSound(Sound.ENDERDRAGON_GROWL).toEveryone().send(message);
            return;
        }

        String message = ChatColor.YELLOW.toString() + ChatColor.BOLD + name + ChatColor.YELLOW + " starts in " + ChatColor.GOLD
                .toString() + ChatColor.BOLD + seconds + ChatColor.YELLOW + " seconds!";
        Notifier.newNotifier().withSound(Sound.CLICK).toEveryone().send(message);
    }
}
